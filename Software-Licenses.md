# Copyright and Licensing

## Content Learning Objectives

By the end of this activity, participants will be able to...

* Explain when a work becomes copyrighted and who owns the copyright.
* Identify what you can and cannot do with unlicensed software.
* Explain the role of licenses.
* Explain the importance of licenses to open-source software.
* Identify the license of published software.
* Articulate why you would or would not be comfortable submitting code to
a project with a particular license.

## Process Skill Goals

During the activity, students should make progress toward:

*

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1

Watch the 40m video "What you need to know about open source licensing" by Felix Crux, presented at PyCon 2016. <https://felixcrux.com/library/what-you-need-to-know-about-open-source-licensing>

### Questions - Part 1

Time 10m

1. You have a new idea for a program, can you copyright that idea?
2. You have written a new program based on that idea, is that software copyright-able?
3. You have written a new program, what do you have to do to copyright that program?
4. If you own the copyrights to something, what can you do that no one else can do? (see [http://www.bitlaw.com/copyright/scope.html](http://www.bitlaw.com/copyright/scope.html))
5. You find the code for a program published on a website. You do not see a copyright symbol, and cannot find a license file in the project. What can you do with it?
6. What is the purpose of a license?
7. When you license your program do you give up your copyright?
8. As the copyright holder, once you license your program, could you later license it under a different license?

(Class discussion 10m)

### Questions - Part 2

Time: 10m

1. If you relicense your program, can someone still use your program under the previous license?
2. Can you sell open-source licensed software?
3. Suppose you are hired to develop software for a company, who owns the copyright on the code you write? What's the license?
4. Suppose you and your friends get together and to write a program collaboratively. Who owns the copyright? Suppose you want to license the software under the MIT license, what must you do?
5. Suppose you start a new project and want to license it under GPL-v3.0. What must you do? If a friend wants to help you with the project, who owns the copyright on what they write? What license is their code licensed under?
6. Try to identify the license for the following projects:
    1. [OpenMRS Core](https://github.com/openmrs/openmrs-core)
    2. [Fineract](https://github.com/apache/fineract)
    3. [VSCodium](https://github.com/VSCodium/vscodium)
    4. [Thea's Pantry GuestBackend](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfobackend)
7. Go to [https://tldrlegal.com/](https://tldrlegal.com/) . Look up each of the above licenses. Identify the “cans” the “cannot(s)” and the “musts” for each.
8. What's the difference between a permissive open-source license and a non-permissive (aka viral or CopyLeft) open-source license?

(Class discussion 10m)

## Model 2

Your team needs to select a license for your project that converts Markdown documents to HTML documents.
You should have a license before you start to add code or documentation to your project.

### Questions

1. **Choose a license for your source code.** Go to [https://choosealicense.com](https://choosealicense.com) and compare the licenses by clicking on the [I want it simple and permissive](https://choosealicense.com/licenses/mit/) and [I care about sharing improvements](https://choosealicense.com/licenses/gpl-3.0/) sections. Compare the licenses, discuss them in your team, and choose one license as a team.
    * Record your license choice here:
    * Explain your team's reasons for choosing this license:
2. **Choose a license for your documentation.** Click on [the link below My project isn't software](https://choosealicense.com/non-software/). Compare the three Creative Commons licenses, discuss them in your team, and choose one license as a team.
    * Record your license choice here:
    * Explain your team's reasons for choosing this license:

---
The content of this activity is based on work by Stoney Jackson and Karl Wurst which is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. The source of the original activity is here: [http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29](http://foss2serve.org/index.php/Intro_to_Copyright_and_Licensing_%28Activity%29)

In accordance with CC-BY-SA 4.0 this derivative is also licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
